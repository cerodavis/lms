<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Data Entry</title>
  <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="Date/css/pickmeup.css" type="text/css"/>

</head>

<style>

.boxs
{
	
	margin-left:1.5%;
	margin-top:1.5%;
	border-style:solid;
	border-color:#000;
	border-width:2%;
	float:left;
	width:500px;
	height:450px;
	font-family:"Adobe Gothic Std B";
	color:#000;
	font-weight:bold;
	font-size:16px;
}

.SnIn
{
	font-family:"Adobe Gothic Std B";
	font-size:16px;
	background-color:#F00;
	color:#FFF;
	border:none;
	height:25px;
	margin-left:350px;
	margin-top:10px;
}


</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.pickmeup.js"></script>
<script type="text/javascript" src="js/demo.js"></script>

<script>

$(function () {
$('.demo').pickmeup({
flat: true
});
var input= $('input');
input.pickmeup({
position: 'right',
before_show: function(){
input.pickmeup('set_date', input.val(), true);
},
change: function(formated){
input.val(formated);
}
});
});

$(function () {
$('.demo').pickmeup({
date: new Date, // Selected date after initialization. Can be single date string/object or array depending on selection mode
flat: false, // Whatever if the date picker is appended to the element or triggered by an event
first_day: 1, // First day of week: 0 - Sunday, 1 - Monday
prev: '&#9664;', // Previous button content
next: '&#9654;', // Next button content
mode: 'single', // Date selection mode. single/multiple/range
view: 'days', // View mode after initialization. days/months/years
calendars: 1, // Number of calendars, that will be rendered
format: 'd-m-Y', // Date format
position: 'bottom', // Date picker's position relative to the triggered element
trigger_event: 'click', // Event to trigger the date picker
class_name: '', // Class to be added to root datepicker element
render: function () {},
change: function () {return true;},
before_show: function () {return true;},
show: function () {return true;},
hide: function () {return true;},
locale: { // Object, that contains localized days of week names and months
days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
}
});
});

</script>


<body>

  <div class="container">
    <div class="boxs">
    KIT MASTER
    
    <form id="kit_master" action="data_entry_php.php" method="post">
    </br>
    Product ID: <input type="text" id="P_ID" name="PID"/>
    </br>
    Supplier Code: <input type="text" id="Supplier_Code" name="Supplier_Code"/>
    </br>
    Branch Code: <input type="text" id="P=Branch_Code" name="Branch_Code"/>
    </br>
    Kit Code: <input type="text" id="Kit_Code" name="Kit_Code"/>
    </br>
    Test ID: <input type="text" id="Test_ID" name="Test_ID"/>
    </br>
    Test: <input type="text" id="Test" name="Test"/>
    </br>
    Machine ID: <input type="text" id="Machine_ID" name="Machine_ID"/>
    </br>
    Calibration ID: <input type="text" id="Calibration_ID" name="Calibration_ID"/>
    </br>
    Control ID: <input type="text" id="Control_ID" name="Control_ID"/>
    </br>
    Calibration Test: <input type="text" id="Calibration_Test" name="Calibration_Test"/>
    </br>
    Control Test: <input type="text" id="Control_Test" name="Control_Test"/>
    </br>
    Usable Test: <input type="text" id="Usable_Test" name="Usable_Test"/>
    </br>
    Use Type: <input type="text" id="Use_Type" name="Use_Type"/>
    </br>
    <input type="submit" value="Enter" class="SnIn" name="kit_master">
    </form>
    </div>
    
    <div class="boxs">
    MACHINE LABNAME
    
    <form id="machine_labname" action="data_entry_php.php" method="post">
    </br>
    Branch ID: <input type="text" id="P_ID" name="Branch_ID"/>
    </br>
    Machine ID: <input type="text" id="Machine_ID" name="Machine_ID"/>
    </br>
    Machine Name: <input type="text" id="Machine_Name" name="Machine_Name"/>
    </br>
    Power Used: <input type="text" id="Power_Used" name="Power_Used"/>
    </br>
    Test ID: <input type="text" id="Test_ID" name="Test_ID"/>
    </br>
    Machine Type: <input type="text" id="Machine_Type" name="Machine_Type"/>
    </br>
    Reagent Type: <input type="text" id="Reagent_Type" name="Reagent_Type"/>
    </br>
    Date Installation: <input type="text" id="Date_Installation" name="Date_Installation"/><div class="demo"></div>
    </br>
    Date Manufacture: <input type="text" id="Date_Manufacture" name="Date_Manufacture"/><div class="demo"></div>
    </br>
    <input type="submit" value="Enter" class="SnIn" name="machine_labname">
    </form>
    </div>
    
    <div class="boxs" style="height:590px">
    SUPPLIER  MASTER LABNAME
    
    <form id="supplier_master_labname" action="data_entry_php.php" method="post">
    </br>
    Product ID: <input type="text" id="P_ID" name="PID"/>
    </br>
    Supplier Name: <input type="text" id="Machine_ID" name="Supplier_Name"/>
    </br>
    Branch Code: <input type="text" id="Machine_Name" name="Branch_Code"/>
    </br>
    Supplier Code: <input type="text" id="Power_Used" name="Supplier_Code"/>
    </br>
    Contact Person: <input type="text" id="Test_ID" name="Contact_Person"/>
    </br>
    Address 1: <input type="text" id="Machine_Type" name="Address_1"/>
    </br>
    Pincode 1: <input type="text" id="Reagent_Type" name="Pincode_1"/>
    </br>
    Address 2: <input type="text" id="Date_Installation" name="Address_2"/>
    </br>
    Pincode 2: <input type="text" id="Date_Manufacture" name="Pincode_2"/>
    </br>
    Phone 1: <input type="text" id="Date_Installation" name="Phone_1"/>
    </br>
    Phone 2: <input type="text" id="Date_Installation" name="Phone_2"/>
    </br>
    Mobile 1: <input type="text" id="Date_Installation" name="Mobile_1"/>
    </br>
    Mobile 2: <input type="text" id="Date_Installation" name="Mobile_2"/>
    </br>
    Email 1: <input type="text" id="Date_Installation" name="Email_1"/>
    </br>
    Email 2: <input type="text" id="Date_Installation" name="Email_2"/>
    </br>
    Website: <input type="text" id="Date_Installation" name="Website"/>
    </br>
    Created By: <input type="text" id="Date_Installation" name="Created_By"/>
    </br>
    Created On: <input type="text" id="Date_Installation" name="Created_On"/><div class="demo"></div>
    </br>
    <input type="submit" value="Enter" class="SnIn" name="supplier_master_labname">
    </form>
    </div>
    
    <div class="boxs" style="height:500px">
    TEST RESULTS LABNAME
    
    <form id="test_results_labname" action="data_entry_php.php" method="post">
    </br>
    Product ID: <input type="text" id="P_ID" name="PID"/>
    </br>
    Patient ID: <input type="text" id="Patient_ID" name="Patient_ID"/>
    </br>
    Patient Mobile: <input type="text" id="Patient_Mobile" name="Patient_Mobile"/>
    </br>
    Client ID: <input type="text" id="Client_ID" name="Client_ID"/>
    </br>
    Doctors ID: <input type="text" id="Doctors_ID" name="Doctors_ID"/>
    </br>
    Branch Code: <input type="text" id="Branch_Code" name="Branch_Code"/>
    </br>
    Association ID: <input type="text" id="ASSOC_ID" name="ASSOC_ID"/>
    </br>
    Group Code Requested: <input type="text" id="Group_Code_Requested" name="Group_Code_Requested"/>
    </br>
    Test ID: <input type="text" id="Test_ID" name="Test_ID"/>
    </br>
    Test Name: <input type="text" id="Test_Name" name="Test_Name"/>
    </br>
    Test Result: <input type="text" id="Test_Result" name="Test_Result"/>
    </br>
    Amount Payable: <input type="text" id="Amount_Payable" name="Amount_Payable"/>
    </br>
    Amount Received: <input type="text" id="Amount_Received" name="Amount_Received"/>
    </br>
    Referral Payable: <input type="text" id="Referral_Payable" name="Referral_Payable"/>
    </br>
    <input type="submit" value="Enter" class="SnIn" name="test_results_labname">
    </form>
    </div>
    
    <div class="boxs" style="height:350px; margin-top:40px;">
    CONSUMABLES MASTER LABNAMES
    
    <form id="consumables_master_labname" action="data_entry_php.php" method="post">
    </br>
    Product ID: <input type="text" id="P_ID" name="PID"/>
    </br>
    Supplier Code: <input type="text" id="Supplier_Code" name="Supplier_Code"/>
    </br>
    Branch Code: <input type="text" id="Branch_Code" name="Branch_Code"/>
    </br>
    Consumable Code: <input type="text" id="Consumable_Code" name="Consumable_Code"/>
    </br>
    Unit: <input type="text" id="Unit" name="Unit"/>
    </br>
    Sub Unit: <input type="text" id="Sub_Unit" name="Sub_Unit"/>
    </br>
    Number Sub Unit: <input type="text" id="Number_Sub_Unit" name="Number_Sub_Unit"/>
    </br>
    Unit Price: <input type="text" id="Unit_Price" name="Unit_Price"/>
    </br>
    <input type="submit" value="Enter" class="SnIn" name="consumables_master_labname">
    </form>
    </div>


  </div>

</body>
</html>